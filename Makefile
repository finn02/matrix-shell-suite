SRCDIR=src
UTILSDIR=$(SRCDIR)/utils
CLIENTSDIR=$(SRCDIR)/HTTP
DOCDIR=doc

SOURCES=$(SRCDIR)/matrix-login \
		$(SRCDIR)/matrix-logout \
		$(SRCDIR)/matrix-post-message \
		$(SRCDIR)/matrix-whoami \
		$(SRCDIR)/matrix-device-keys \
		$(SRCDIR)/matrix-joined-rooms

UTILS=$(UTILSDIR)/cookie.sh      \
	  $(UTILSDIR)/http_common.sh \
	  $(UTILSDIR)/optparse.sh    \
	  $(UTILSDIR)/functions.sh

CLIENTS=$(CLIENTSDIR)/wget.sh \
		$(CLIENTSDIR)/curl.sh

.PHONY=all
all: lint

.PHONY=check
check: unit-tests repo-tests lint

TESTS_DIR=unit-tests
.PHONY=unit-tests
unit-tests: $(SOURCES) $(UTILS) $(CLIENTS)
	$(MAKE) -C $(TESTS_DIR)

.PHONY=repo-tests
repo-tests: $(SOURCES) $(UTILS) $(CLIENTS)
	./tests/check-makefile-sources $?
	./tests/http-client-api

.PHONY=lint
lint: $(SOURCES) $(UTILS) $(CLIENTS)
	echo "Running shellcheck..."
	shellcheck -x $?


.PHONY=FORCE
FORCE:

doc: $(SOURCES) $(UTILS) $(CLIENTS) FORCE
	$(MAKE) -C $(DOCDIR)

ifndef VERBOSE
.SILENT:
endif

.POSIX:
