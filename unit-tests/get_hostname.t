#!/usr/bin/env bash

test_description="Test the _get_hostname function"

. ./sharness.sh

set -e

if [[ -z $SHARNESS_BUILD_DIRECTORY ]]; then
	exit 1
fi

source "$SHARNESS_BUILD_DIRECTORY/src/utils/functions.sh"

test_expect_success "DNS Name" "test $(_get_hostname matrix.org) = matrix.org"
test_expect_success "DNS Name with Port" "test $(_get_hostname matrix.org:8000) = matrix.org"
test_expect_success "IPv4 Literal" "test $(_get_hostname "1.2.3.4") = 1.2.3.4"
test_expect_success "IPv4 Literal with Port" "test $(_get_hostname "1.2.3.4:1234") = 1.2.3.4"
test_expect_success "IPv6 Literal" "test $(_get_hostname "[1234:5678::abcd]") = '[1234:5678::abcd]'"
test_expect_success "IPv6 Literal with Port" "test $(_get_hostname "[1234:5678::abcd]:5678") = [1234:5678::abcd]"

test_done
