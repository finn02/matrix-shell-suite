#!/usr/bin/env bash

test_description="Test the get_homeserver_url function"

. ./sharness.sh

set -e

if [[ -z $SHARNESS_BUILD_DIRECTORY ]]; then
	exit 1
fi

source "$SHARNESS_BUILD_DIRECTORY/src/utils/functions.sh"

default_hs="https://example.com:443"
otherport_hs="https://example.com:8432"

test_expect_success "Test sanitizing protocol" "
test $(get_homeserver_url example.com) = $default_hs &&
test $(get_homeserver_url http://example.com) = $default_hs &&
test $(get_homeserver_url https://example.com) = $default_hs
"

test_expect_success "Test sanitizing port" "
test $(get_homeserver_url http://example.com:443) = $default_hs &&
test $(get_homeserver_url http://example.com:8432) = $otherport_hs
"
test_done
