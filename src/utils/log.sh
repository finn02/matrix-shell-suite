#!/usr/bin/env sh

# Copyright (c) 2019, Joaquín P. Centeno
# Copyright (c) 2020, Darshit Shah
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Logging levels
LOGSH_LEVEL_EXTREME=3
LOGSH_LEVEL_TRACE=5
LOGSH_LEVEL_DEBUG=10
LOGSH_LEVEL_INFO=20
LOGSH_LEVEL_WARNING=30
LOGSH_LEVEL_ERROR=40
LOGSH_LEVEL_CRITICAL=50

# Default logging level:
LOGSH_LEVEL="${LOGSH_LEVEL:-$LOGSH_LEVEL_INFO}"

# Logging colors
_LOGSH_CO_EXTREME="\033[35m"
_LOGSH_CO_DEBUG="\033[34m" # blue fg
_LOGSH_CO_INFO="\033[32m" # green fg
_LOGSH_CO_WARNING="\033[33m" # yellow fg
_LOGSH_CO_ERROR="\033[31m" # red fg
_LOGSH_CO_CRITICAL="\033[1;31m" # bold red fg
_LOGSH_CO_RESET="\033[0m"

# Logging level names
_LOGSH_LVLNAME_EXTREME="DEEP_TRACE"
_LOGSH_LVLNAME_DEBUG="DEBUG"
_LOGSH_LVLNAME_INFO="INFO"
_LOGSH_LVLNAME_WARNING="WARNING"
_LOGSH_LVLNAME_ERROR="ERROR"
_LOGSH_LVLNAME_CRITICAL="CRITICAL"

# Internal function for logging.
# Usage: _log <level> <level name> <level color> message...
_log() {
    local TIMESTAMP
    TIMESTAMP="$(date +'%Y-%m-%d %H:%M:%S.%N')"
    local LEVEL="$1"
    local LEVEL_NAME="$2"
    local LEVEL_COLOR="$3"
    shift 3 # Now $@ contains just the message

    # Skip if logging level for this message is lower than the minimum.
    # Return 0 to prevent breaking scripts with `set -e`.
    [ "$LOGSH_LEVEL" -le "$LEVEL" ] || return 0
    >&2 printf "${LEVEL_COLOR}[%s][%s]${_LOGSH_CO_RESET} %s\n" "$TIMESTAMP" "$LEVEL_NAME" "$*"
}

# Public logging functions:

log_debug() {
    _log "$LOGSH_LEVEL_DEBUG" "$_LOGSH_LVLNAME_DEBUG" "$_LOGSH_CO_DEBUG" "$@"
}

log_info() {
    _log "$LOGSH_LEVEL_INFO" "$_LOGSH_LVLNAME_INFO" "$_LOGSH_CO_INFO" "$@"
}

log_warning() {
    _log "$LOGSH_LEVEL_WARNING" "$_LOGSH_LVLNAME_WARNING" "$_LOGSH_CO_WARNING" "$@"
}

log_error() {
    _log "$LOGSH_LEVEL_ERROR" "$_LOGSH_LVLNAME_ERROR" "$_LOGSH_CO_ERROR" "$@"
}

log_critical() {
    _log "$LOGSH_LEVEL_CRITICAL" "$_LOGSH_LVLNAME_CRITICAL" "$_LOGSH_CO_CRITICAL" "$@"
}

log_extreme() {
    _log "$LOGSH_LEVEL_EXTREME" "$_LOGSH_LVLNAME_EXTREME" "$_LOGSH_CO_EXTREME" "$@"
}
