#!/usr/bin/env sh

#  Query current devices and identity keys for the current user
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

## @file matrix-whoami
## @brief Print a list of all devices and E2EE keys for the logged in user.
##
## \param[in] cookie_file. Path to matrix-cookie file (optional)
## \return Details about devices of currently logged-in user
##
## Queries the homeserver using the \p cookie_file for information about the
## user's devices and their keys.
## If \p cookie_file is not specified, the default path at #MATRIX_COOKIE is
## used.
##
## The REST endpoint used here, supports querying for any user. So, in the
## future, this script will hopefully support querying other users' information
## as well
##
## Example:
## ```
## $ matrix-devices
## {
##   "device_keys": {
##     "@testuser:matrix.org": {
##       "ABCDEFGHIJ": {
##         "algorithms": [
##           "m.olm.v1.curve25519-aes-sha2",
##           "m.megolm.v1.aes-sha2"
##         ],
##         "device_id": "ABCDEFGHIJ",
##         "keys": {
##           "curve25519:ABCDEFGHIJ": "<curve25519 key>",
##           "ed25519:ABCDEFGHIJ": "<ed25519 key>"
##         },
##         "signatures": {
##           "@testuser:matrix.org": {
##             "ed25519:ABCDEFGHIJ": "<ed25519 signature>"
##           }
##         },
##         "user_id": "@testuser:matrix.org",
##         "unsigned": {
##           "device_display_name": "A client"
##         }
##       },
##       "FOOBARABCD": {
##         "algorithms": [
##           "m.megolm.v1.aes-sha2",
##           "m.olm.v1.curve25519-aes-sha2"
##         ],
##         "device_id": "FOOBARABCD",
##         "keys": {
##           "curve25519:FOOBARABCD": "<curve25519 key>",
##           "ed25519:FOOBARABCD": "<ed25519 key>"
##         },
##         "signatures": {
##           "@testuser:matrix.org": {
##             "ed25519:FOOBARABCD": "<ed25519 signature>"
##           }
##         },
##         "user_id": "@testuser:matrix.org",
##         "unsigned": {
##           "device_display_name": "B Client"
##         }
##       }
##     }
##   },
## ```
##
## @author Darshit Shah <git@darnir.net>
## @copyright GNU GPLv3+

set -e
set -u

script_dir=$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd -P)

#shellcheck source=./src/utils/optparse.sh
. "${script_dir}/utils/optparse.sh"
#shellcheck source=./src/utils/functions.sh
. "${script_dir}/utils/functions.sh"
#shellcheck source=./src/utils/cookie.sh
. "${script_dir}/utils/cookie.sh"

if [ $# -gt 1 ]; then
	>&2 echo "Usage: $0 [cookie_file]"
	exit 1
fi

exists jq

cookie_file=${2:-$MATRIX_COOKIE}
verify_cookie_exists "$cookie_file"

homeserver="$(get_homeserver_base "$cookie_file")"
auth_token="$(get_auth_token "$cookie_file")"
user_id="$(get_user_id "$cookie_file")"

REST_ENDPOINT="_matrix/client/r0/keys/query"
HTTP_ENDPOINT="${homeserver}/${REST_ENDPOINT}"

DATA="$(printf '{"device_keys": {"%s": []}}' "$user_id")"

HPOST_AUTH "$HTTP_ENDPOINT" "$auth_token" "$DATA" | jq '.'
