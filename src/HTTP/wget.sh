#!/usr/bin/env sh

#  Wget Abstractions for Matrix Shell Suite
#  Copyright (C) 2019 Darshit Shah <git@darnir.net>
#
# This file is part of the Matrix Shell Suite
#
# Matrix Shell Suite is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Matrix Shell Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Matrix Shell Suite.  If not, see <http://www.gnu.org/licenses/>.

# shellcheck source=./src/utils/http_common.sh
. src/utils/http_common.sh

exists wget

if [ -n "${DEBUG_MATRIX:-}" ]; then
	DEBUG_PARAMS="--debug"
else
	DEBUG_PARAMS=""
fi

WGET_PARAMS="-O- --quiet ${DEBUG_PARAMS} --content-on-error --no-config --server-response"

__call_wget() {
	tmpfile=$(mktemp)

	#shellcheck disable=SC2086
	command wget -d -o "$tmpfile" $WGET_PARAMS "$@" 2>/dev/null

	ret=$(http_to_retcode "$(grep -e "HTTP/1.1" -e "HTTP/2" "$tmpfile" | tail -1 | awk '{print $2}')")
	log_extreme "$(cat $tmpfile)"
	rm "$tmpfile"
	return "$ret"
}

## HTTP GET
##############################################################################

__get() {
	URL="$1" && shift

	__call_wget "$@" "$URL"
}

HGET() {
	__get "$1"
}

HGET_AUTH() {
	URL="$1"
	auth_token="$2"

	__get "$URL" --header="Authorization: Bearer $auth_token"
}

## HTTP POST
##############################################################################

__post() {
	URL="$1" && shift
	DATA="$1" && shift

	__call_wget --method=POST --body-data "$DATA" "$@" "$URL"
}

HPOST() {
	URL="$1"
	DATA="$2"

	__post "$URL" "$DATA"
}

HPOST_AUTH() {
	URL="$1" && shift
	auth_token="$1" && shift
	DATA=$*

	__post "$URL" "$DATA" --header="Authorization: Bearer $auth_token"

}

## HTTP POST
##############################################################################

__put() {
	URL="$1" && shift
	DATA="$1" && shift

	#shellcheck disable=SC2086
	__call_wget --method=PUT --header="Content-Type: application/json" --body-data "$DATA" "$@" "$URL"
}

HPUT() {
	URL="$1"
	DATA="$2"

	__put "$URL" "$DATA"
}

HPUT_AUTH() {
	URL="$1" && shift
	auth_token="$1" && shift
	DATA=$*

	__put "$URL" "$DATA" --header="Authorization: Bearer $auth_token"
}
