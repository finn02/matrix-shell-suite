For any HTTP Request, the following conventions should be followed:

  1. All "public" methods will accept _no_ extra arguments for setting
     command line parameters. This is to prevent accidentally breaking the
     abstraction and passing options that are valid only for a subset of HTTP
     programs
  2. All "public" methods will return the content of the file on stdout and
     the last HTTP Response code on stderr
  3. An implicit requirement of the above rule is that all "public" methods
     will accept and send exactly 1 HTTP Request
